-module(task, [Id, TaskText]).
-compile(export_all).

validation_tests() ->
  [{fun() -> length(TaskText) > 0 end,
    "Text must be non-empty!"},
   {fun() -> length(TaskText) =< 140 end,
    "Text must be tweetable!"}].
