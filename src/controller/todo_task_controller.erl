-module(todo_task_controller, [Req]).
-compile(export_all).

list('GET', []) ->
  Tasks = boss_db:find(task, []),
  {ok, [{tasks, Tasks}]}.

create('GET', []) ->
  ok;
create('POST', []) ->
  TaskText = Req:post_param("task_text"),
  NewTask = task:new(id, TaskText),
  case NewTask:save() of
    {ok, SavedTask} ->
      {redirect, [{action, "list"}]};
    {error, ErrorList} ->
      {ok, [{errors, ErrorList}, {new_msg, NewTask}]}
  end.

goodbye('POST', []) ->
  boss_db:delete(Req:post_param("task_id")),
  {redirect, [{action, "list"}]}.

pull('GET', [LastTimestamp]) ->
  {ok, Timestamp, Tasks} = boss_mq:pull("new-tasks", list_to_integer(LastTimestamp)),
  {json, [{timestamp, Timestamp}, {tasks, Tasks}]}.

live('GET', []) ->
  Tasks = boss_db:find(task, []),
  Timestamp = boss_mq:now("new-tasks"),
  {ok, [{tasks, Tasks}, {timestamp, Timestamp}]}.
